# StudyPal Bot
### AP Project | Kelompok 10 | Adpro-C
[![pipeline status](https://gitlab.com/ap-kelompok-10/studypal-todolist/badges/master/pipeline.svg)](https://gitlab.com/ap-kelompok-10/studypal-todolist/-/commits/master) [![coverage report](https://gitlab.com/ap-kelompok-10/studypal-todolist/badges/master/coverage.svg)](https://gitlab.com/ap-kelompok-10/studypal-todolist/-/commits/master)

## Anggota
    1. Astrida Nayla
    2. Reka Paska Enda
    3. Fathur Rahman Prawira
    4. Muhammad Aulia Akbar

## Tentang Project
Seringkali pelajar memiliki banyak sekali tugas yang harus dikerjakan. Walau sudah mencatat di notes, tapi jarang dilihat. Bagaimana mengingatkan mereka akan tugas tersebut melalui media yang sering mereka gunakan? 

StudyPal merupakan sebuah bot LINE yang memenuhi kebutuhan pelajar dalam aktivitas pembelajaran. Dengan Studypal, pelajar dapat menambahkan to-do-list mereka sendiri dan mengatur reminder untuk setiap kegiatan to-do tersebut.

## Kunjungi Bot
Silahkan mencoba bot kami dengan menambahnya sebagai teman di line dengan kode berikut.

![alt text](https://i.ibb.co/wynnm4k/329rhwzo.png)