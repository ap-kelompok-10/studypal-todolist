package com.studypal.todolist.service.todolist;

import com.studypal.todolist.core.bot.Bot;
import com.studypal.todolist.core.todo.Todo;
import com.studypal.todolist.core.todo.TodoStudy;
import com.studypal.todolist.core.todo.TodoTask;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TodolistServiceTest {
    private Class<?> todolistServiceClass;

    @BeforeEach
    public void setUp() throws Exception {
        todolistServiceClass = Class.forName("com.studypal.todolist.service."
                + "todolist.TodolistService");
    }

    @Test
    public void todolistServiceIsAPublicInterface() {
        int classModifiers = todolistServiceClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testHasAddTodoTaskAbstractMethod() throws Exception {
        Method cast = todolistServiceClass.getDeclaredMethod("addTodoTask", TodoTask.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

    @Test
    public void testHasAddTodoStudyAbstractMethod() throws Exception {
        Method cast = todolistServiceClass.getDeclaredMethod("addTodoStudy", TodoStudy.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

    @Test
    public void testHasChecklistTaskAbstractMethod() throws Exception {
        Method cast = todolistServiceClass.getDeclaredMethod("checklistTask", String.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

    @Test
    public void testHasChecklistStudyAbstractMethod() throws Exception {
        Method cast = todolistServiceClass.getDeclaredMethod("checklistStudy", String.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

    @Test
    public void testHasGetRemainingTimeAbstractMethod() throws Exception {
        Method cast = todolistServiceClass.getDeclaredMethod("getRemainingTime", TodoTask.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

    @Test
    public void testHasGetTodoStudiesAbstractMethod() throws Exception {
        Method cast = todolistServiceClass.getDeclaredMethod("getTodoStudies", String.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

    @Test
    public void testHasGetTodoTasksAbstractMethod() throws Exception {
        Method cast = todolistServiceClass.getDeclaredMethod("getTodoTasks", String.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

    @Test
    public void testHasGetBotByIdAbstractMethod() throws Exception {
        Method cast = todolistServiceClass.getDeclaredMethod("getBotById", String.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

    @Test
    public void testHasGetTaskByEntryAbstractMethod() throws Exception {
        Method cast = todolistServiceClass.getDeclaredMethod("getTodoTaskByEntry", String.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

    @Test
    public void testHasGetStudyByEntryAbstractMethod() throws Exception {
        Method cast = todolistServiceClass.getDeclaredMethod("getTodoStudyByEntry", String.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

    @Test
    public void testHasAddBotAbstractMethod() throws Exception {
        Method cast = todolistServiceClass.getDeclaredMethod("addBot", Bot.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }
}
