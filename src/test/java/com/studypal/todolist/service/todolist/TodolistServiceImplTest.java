package com.studypal.todolist.service.todolist;

import com.studypal.todolist.core.bot.Bot;
import com.studypal.todolist.core.todo.TodoStudy;
import com.studypal.todolist.core.todo.TodoTask;
import com.studypal.todolist.repository.BotRepository;
import com.studypal.todolist.repository.TodoStudyRepository;
import com.studypal.todolist.repository.TodoTaskRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class TodolistServiceImplTest {
    @Mock
    TodoTaskRepository todoTaskRepository;

    @Mock
    TodoStudyRepository todoStudyRepository;

    @Mock
    BotRepository botRepository;

    @Mock
    List<TodoTask> tasks;

    @Mock
    HashMap<String, List<TodoTask>> taskMap;

    @InjectMocks
    TodolistServiceImpl todolistService;

    TodoTask todoTask;
    TodoStudy todoStudy;
    Bot bot;

    @BeforeEach
    public void setUp() {
        bot = new Bot("Dummy", "f", "casual", "ID01", "Astrid");
        todoTask = new TodoTask("ID01", "Tutorial Adpro", "Advanced Programming",
                LocalDateTime.of(2020, 9,
                        10, 6, 40, 45));
        todoStudy = new TodoStudy("ID01", "Belajar Adpro", 2,
                LocalDateTime.of(2020, 9, 10, 6,
                        40, 45));
        tasks = new ArrayList<>();
        tasks.add(todoTask);
        taskMap.put("Advanced Programming", tasks);
    }

    @Test
    public void testParameterlessConstructor() {
        todolistService = new TodolistServiceImpl();
        assertEquals(true, true);
    }

    @Test
    public void testGetBotById() {
        lenient().when(todolistService.getBotById("ID01")).thenReturn(bot);
    }

    @Test
    public void testAddBot() {
        todolistService.addBot(bot);
        lenient().when(todolistService.addBot(bot)).thenReturn(bot);
    }

    @Test
    public void testAddTodoTask() {
        todolistService.addTodoTask(todoTask);
        lenient().when(todolistService.addTodoTask(todoTask)).thenReturn(todoTask);
    }

    @Test
    public void testAddTodoStudy() {
        todolistService.addTodoStudy(todoStudy);
        lenient().when(todolistService.addTodoStudy(todoStudy)).thenReturn(todoStudy);
    }

    @Test
    public void testGetTodoTaskByEntry() {
        lenient().when(todolistService.getTodoTaskByEntry("Tutorial Adpro")).thenReturn(todoTask);
    }

    @Test
    public void testGetTodoStudyByEntry() {
        lenient().when(todolistService.getTodoStudyByEntry("Belajar Adpro")).thenReturn(todoStudy);
    }

    @Test
    public void testGetTodoTasks() {
        lenient().when(todolistService.getTodoTasks("ID01").get("Belajar Adpro")).thenReturn(tasks);
    }

    @Test
    public void testHashMapMaker() {
        lenient().when(todolistService.taskHashMapMaker(tasks)).thenReturn(taskMap);
    }

    @Test
    public void testHashMapMakerAlreadyConsisted() {
        tasks.add(todoTask);
        lenient().when(todolistService.taskHashMapMaker(tasks)).thenReturn(taskMap);
    }

    @Test
    public void testGetTodoStudy() {
        List<TodoStudy> studies = todoStudyRepository.fetchStudies("ID01");
        lenient().when(todolistService.getTodoStudies("ID01")).thenReturn(studies);
    }

    @Test
    public void testChecklistTask() {
        todolistService.addTodoTask(todoTask);
        todolistService.checklistTask("Tutorial Adpro");
        String entryName = todoTask.getEntryName();
        lenient().when(todoTaskRepository.findById(entryName)).thenReturn(null);
    }

    @Test
    public void testChecklistStudy() {
        todolistService.addTodoStudy(todoStudy);
        todolistService.checklistStudy("Belajar Adpro");
        String entryName = todoStudy.getEntryName();
        lenient().when(todoStudyRepository.findById(entryName)).thenReturn(null);
    }

    @Test
    public void testGetRemainingTime() {
        long remainingTime = todolistService.getRemainingTime(todoTask);
        assertTrue(remainingTime > 43200);
    }
}
