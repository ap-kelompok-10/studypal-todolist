package com.studypal.todolist.service.flex;

import com.studypal.todolist.core.bot.Bot;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class BotProfileFlexTest {
    Bot bot;

    @BeforeEach
    public void setUp() {
        bot = new Bot("Dummy", "f", "casual", "123", "Astrid");
    }

    @Test
    public void testFlexCall() {
        BotProfileFlexMessage profileFlexMessage = new BotProfileFlexMessage(
                bot.getName(),
                bot.getGender(),
                bot.getMotivationType(),
                bot.getOwnerName(),
                bot.getSticker()
        );
        profileFlexMessage.get();
        assertEquals(true, true);
    }

    @Test
    public void testFlexFalseStickerCall() {
        bot.changeSticker("fire");
        BotProfileFlexMessage profileFlexMessage = new BotProfileFlexMessage(
                bot.getName(),
                bot.getGender(),
                bot.getMotivationType(),
                bot.getOwnerName(),
                bot.getSticker()
        );
        profileFlexMessage.get();
        assertEquals(true, true);
    }

    @Test
    public void testFlexTrueStickerCall() {
        bot.changeSticker("ok");
        BotProfileFlexMessage profileFlexMessage = new BotProfileFlexMessage(
                bot.getName(),
                bot.getGender(),
                bot.getMotivationType(),
                bot.getOwnerName(),
                bot.getSticker()
        );
        profileFlexMessage.get();
        assertEquals(true, true);
    }

    @Test
    public void testFlexMaleCall() {
        bot.changeGender("m");
        BotProfileFlexMessage profileFlexMessage = new BotProfileFlexMessage(
                bot.getName(),
                bot.getGender(),
                bot.getMotivationType(),
                bot.getOwnerName(),
                bot.getSticker()
        );
        profileFlexMessage.get();
        assertEquals(true, true);
    }
}
