package com.studypal.todolist.service.flex;

import com.studypal.todolist.core.todo.TodoStudy;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class TodostudyFlexTest {
    TodoStudy todoStudy;
    List<TodoStudy> todoStudies;

    @BeforeEach
    public void setUp() {
        todoStudy = new TodoStudy("ID01", "Belajar Adpro", 2,LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        todoStudies = new ArrayList<>();
        todoStudies.add(todoStudy);
    }

    @Test
    public void testFlexCall() {
        TodostudyFlexMessage todolistFlexMessage = new TodostudyFlexMessage(todoStudies);
        todolistFlexMessage.get();
        assertEquals(true, true);
    }
}
