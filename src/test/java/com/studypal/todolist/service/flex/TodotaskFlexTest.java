package com.studypal.todolist.service.flex;

import com.studypal.todolist.core.todo.TodoTask;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class TodotaskFlexTest {
    TodoTask todoTask;
    HashMap<String, List<TodoTask>> todoTasks;

    @BeforeEach
    public void setUp() {
        todoTask = new TodoTask("ID01", "Tutorial Adpro",
                "Advanced Programming", LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        todoTasks = new HashMap<>();
        List<TodoTask> tasks = new ArrayList<>();
        tasks.add(todoTask);
        todoTasks.put(todoTask.getCourse(), tasks);
    }

    @Test
    public void testFlexCall() {
        TodotaskFlexMessage todolistFlexMessage = new TodotaskFlexMessage(todoTasks);
        todolistFlexMessage.get();
        assertEquals(true, true);
    }
}
