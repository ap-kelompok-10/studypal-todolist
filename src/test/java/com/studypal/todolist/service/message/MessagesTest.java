package com.studypal.todolist.service.message;

import com.studypal.todolist.core.reminder.Reminder;

import com.linecorp.bot.model.message.TextMessage;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class MessagesTest {
    @Spy
    Messages message;

    @Test
    public void contextLoads() throws Exception {
        assertNotNull(message);
    }

    @Test
    public void testGetMotivation() {
        assertTrue(message.getMotivation("casual") instanceof TextMessage);
    }

    @Test
    public void testGenerateReminderResponseEmpty() {
        List<Reminder> reminderList = new ArrayList<Reminder>();
        TextMessage expected = new TextMessage("you have no reminder.");
        TextMessage actual = message.generateReminderResponse(reminderList);
        assertEquals(expected, actual);
    }

    @Test
    public void testGenerateReminderResponseWithResponse() {
        List<Reminder> reminderList = new ArrayList<Reminder>();
        reminderList.add(new Reminder("123", new Date(), "Kerjain Tugas"));
        String pattern = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        TextMessage expected = new TextMessage("list of reminder:\n"
                + "id=null | Kerjain Tugas | remind date: " + date);
        TextMessage actual = message.generateReminderResponse(reminderList);
        assertEquals(expected, actual);
    }
}
