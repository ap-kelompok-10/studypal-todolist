package com.studypal.todolist.service.reminder;

import com.studypal.todolist.core.reminder.Reminder;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Date;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReminderServiceTest {
    private Class<?> reminderServiceClass;

    @BeforeEach
    public void setUp() throws Exception {
        reminderServiceClass = Class.forName("com.studypal.todolist.service."
                + "reminder.ReminderService");
    }

    @Test
    public void reminderServiceIsAPublicInterface() {
        int classModifiers = reminderServiceClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testHasAddReminderAbstractMethod() throws Exception {
        Method cast = reminderServiceClass.getDeclaredMethod("addReminder", Reminder.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

    @Test
    public void testHasGetReminderByDateAbstractMethod() throws Exception {
        Method cast = reminderServiceClass.getDeclaredMethod("getReminderByDate", Date.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }
}
