package com.studypal.todolist.service.reminder;

import com.studypal.todolist.core.reminder.Reminder;
import com.studypal.todolist.repository.ReminderRepository;

import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class ReminderServiceImplTest {
    @Mock
    ReminderRepository reminderRepository;

    @Mock
    List<Reminder> listReminder;

    @InjectMocks
    ReminderServiceImpl reminderService;

    Reminder reminder;

    @BeforeEach
    public void setUp() {
        reminder = new Reminder("123", new Date(), "Kerjain Tugas");
        listReminder.add(reminder);
    }

    @Test
    public void testParameterlessConstructor() {
        reminderService = new ReminderServiceImpl();
        assertEquals(true, true);
    }

    @Test
    public void testAddReminder() {
        reminderService.addReminder(reminder);
        lenient().when(reminderService.addReminder(reminder)).thenReturn(reminder);
    }

    @Test
    public void testGetReminderByDate() {
        lenient().when(reminderService.getReminderByDate(new Date())).thenReturn(listReminder);
    }

    @Test
    public void testGetReminderByOwnerId() {
        lenient().when(reminderService.getReminderByOwnerId("123")).thenReturn(listReminder);
    }

    @Test
    public void testDeleteReminderById() {
        reminderService.deleteReminderById("123", 1);
    }
}
