package com.studypal.todolist.component;

import com.studypal.todolist.core.reminder.Reminder;
import com.studypal.todolist.service.reminder.ReminderService;
import com.studypal.todolist.service.todolist.TodolistServiceImpl;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class ReminderCronTest {
    @Mock
    private LineMessagingClient lineMessagingClient;

    @Mock
    private CompletableFuture<BotApiResponse> botresp;

    @Mock
    Reminder reminder;

    @Mock
    ReminderService reminderService;

    @InjectMocks
    TodolistServiceImpl todolistServiceImpl;

    @Spy
    @InjectMocks
    ReminderCron reminderCron;

    @BeforeEach
    public void setUp() {
        reminder = new Reminder("123", new Date(), "Kerjain Tugas");
    }

    @Test
    public void testCreateTextMessage() {
        TextMessage expected = new TextMessage("reminder:\n"
                + "Gosok gigi");
        TextMessage actual = reminderCron.createTextMessage("Gosok gigi");
        assertEquals(expected, actual);
    }

    @Test
    public void testSendReminder() throws Exception {
        List<Reminder> allDueReminder  = new ArrayList<>();
        allDueReminder.add(reminder);
        if (allDueReminder.size() > 0) {
            lenient().when(reminderService.getReminderByDate(Calendar.getInstance()
                    .getTime())).thenReturn(allDueReminder);
            lenient().when(lineMessagingClient.pushMessage(any(PushMessage.class))).thenReturn(botresp);
            reminderCron.sendReminder();
            Mockito.verify(reminderCron).sendReminder();
        } else {
            throw new Exception();
        }
    }

    @Test
    public void testCronReplies() throws Exception {
        List<Reminder> allDueReminder  = new ArrayList<>();
        allDueReminder.add(reminder);
        if (allDueReminder.size() > 0) {
            lenient().when(reminderService.getReminderByDate(Calendar.getInstance().getTime()))
                    .thenReturn(allDueReminder);
            lenient().when(lineMessagingClient.pushMessage(any(PushMessage.class))).thenReturn(botresp);
            reminderCron.cronReplies(allDueReminder);
            Mockito.verify(reminderCron).cronReplies(allDueReminder);
        } else {
            throw new Exception();
        }
    }

    @Test
    public void testCronRepliesNull() throws Exception {
        List<Reminder> allDueReminder  = new ArrayList<>();
        allDueReminder.add(null);
        if (allDueReminder.size() > 0) {
            lenient().when(reminderService.getReminderByDate(Calendar.getInstance().getTime()))
                    .thenReturn(allDueReminder);
            lenient().when(lineMessagingClient.pushMessage(any(PushMessage.class))).thenReturn(botresp);
            reminderCron.cronReplies(allDueReminder);
            Mockito.verify(reminderCron).cronReplies(allDueReminder);
        } else {
            throw new Exception();
        }
    }
}
