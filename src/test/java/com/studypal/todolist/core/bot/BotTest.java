package com.studypal.todolist.core.bot;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
public class BotTest {
    Bot bot;

    @BeforeEach
    public void setUp() {
        bot = new Bot("Dummy", "f", "casual", "123", "Astrid");
    }

    @Test
    public void testChangeName() {
        bot.changeName("Derp");
        assertEquals("Derp", bot.getName());
    }

    @Test
    public void testChangeStickerFalse() {
        bot.changeSticker("fire");
        assertEquals("false", bot.getSticker());
    }

    @Test
    public void testChangeStickerTrue() {
        bot.changeSticker("ok");
        assertEquals("true", bot.getSticker());
    }

    @Test
    public void testChangeStickerInvalid() {
        bot.changeSticker("no");
        assertEquals("false", bot.getSticker());
    }

    @Test
    public void testChangeGenderMale() {
        bot.changeGender("m");
        assertEquals("Male", bot.getGender());
    }

    @Test
    public void testInvalidChangeGender() {
        bot.changeGender("wanita");
        assertEquals("Female", bot.getGender());
    }

    @Test
    public void testChangeGenderFemale() {
        Bot maleBot = new Bot("Dummy", "m", "tough", "123", "Astrid");
        maleBot.changeGender("f");
        assertEquals("Female", maleBot.getGender());
    }

    @Test
    public void testChangeBehaviour() {
        bot.changeMotivationType("tough");
        assertEquals("tough", bot.getMotivationType());
    }

    @Test
    public void testChangeBehaviourCasual() {
        bot.changeMotivationType("casual");
        assertEquals("casual", bot.getMotivationType());
    }

    @Test
    public void testInvalidChangeBehaviour() {
        bot.changeMotivationType("cute");
        assertEquals("casual", bot.getMotivationType());
    }

    @Test
    public void testCreateMaleBot() {
        Bot maleBot = new Bot("Dummy", "m", "tough", "123", "Astrid");
        assertEquals("Male", maleBot.getGender());
    }

    @Test
    public void testCreateInvalidGenderBot() {
        Bot maleBot = new Bot("Dummy", "cowok", "tough", "123", "Astrid");
        assertEquals(null, maleBot.getGender());
    }

    @Test
    public void testGetName() {
        assertEquals("Dummy", bot.getName());
    }

    @Test
    public void testGetSticker() {
        assertEquals("false", bot.getSticker());
    }

    @Test
    public void testGetGender() {
        assertEquals("Female", bot.getGender());
    }

    @Test
    public void testGetCasualMotivation() {
        assertNotNull(bot.getMotivation());
    }

    @Test
    public void testGetToughMotivation() {
        bot.changeMotivationType("tough");
        assertNotNull(bot.getMotivation());
    }

    @Test
    public void testGetBehaviour() {
        assertEquals("casual", bot.getMotivationType());
    }

    @Test
    public void testGetOwnerName() {
        assertEquals("Astrid", bot.getOwnerName());
    }

    @Test
    public void testCheckSticker() {
        assertEquals(false, bot.checkSticker());
    }

    @Test
    public void testCheckStickerTrue() {
        bot.changeSticker("ok");
        assertEquals(true, bot.checkSticker());
    }

    @Test
    public void testParameterlessConstructor() {
        bot = new Bot();
        assertNull(bot.getName());
    }
}
