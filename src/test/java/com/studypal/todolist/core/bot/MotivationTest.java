package com.studypal.todolist.core.bot;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MotivationTest {
    private Class<?> motivationClass;

    @BeforeEach
    public void setUp() throws Exception {
        motivationClass = Class.forName("com.studypal.todolist.core.bot.Motivation");
    }

    @Test
    public void testMotivationIsAPublicInterface() {
        int classModifiers = motivationClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testHasGetMotivationAbstractMethod() throws Exception {
        Method cast = motivationClass.getDeclaredMethod("getMotivation");
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, cast.getParameterCount());
    }
}
