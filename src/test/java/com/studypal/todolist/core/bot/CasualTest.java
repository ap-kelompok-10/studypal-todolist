package com.studypal.todolist.core.bot;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class CasualTest {
    Casual casualBehaviour;

    @BeforeEach
    public void setUp() {
        casualBehaviour = new Casual();
    }

    @Test
    public void testGetMotivation() {
        assertNotNull(casualBehaviour.getMotivation());
    }
}
