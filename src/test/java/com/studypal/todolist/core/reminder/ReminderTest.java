package com.studypal.todolist.core.reminder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class ReminderTest {
    Reminder reminder;

    @BeforeEach
    public void setUp() {
        reminder = new Reminder("123", new Date(), "Kerjain Tugas");
    }

    @Test
    public void testParameterlessConstructor() {
        reminder = new Reminder();
        assertNull(reminder.getOwnerId());
    }

    @Test
    public void testGetDescription() {
        assertEquals("Kerjain Tugas", reminder.getDescription());
    }

    @Test
    public void testGetOwnerId() {
        assertEquals("123", reminder.getOwnerId());
    }

    @Test
    public void testParseFromString() {
        Date date = new GregorianCalendar(2020, 10, 10).getTime();
        assertEquals(date, Reminder.parseFromString("10-10-2020"));
    }

    @Test
    public void testParseFromStringDateOnly() {
        Date date = new GregorianCalendar(2020, 10, 30).getTime();
        Date dummy = Reminder.parseFromString("30");
        dummy.setMonth(date.getMonth());
        dummy.setYear(date.getYear());
        assertEquals(date, dummy);
    }

    @Test
    public void testParseFromStringNoYear() {
        Date date = new GregorianCalendar(2020, 10, 10).getTime();
        Date dummy = Reminder.parseFromString("10-10");
        dummy.setYear(date.getYear());
        assertEquals(date, dummy);
    }

    @Test
    public void testInvalidStringException() {
        assertThrows(IndexOutOfBoundsException.class, () -> {
            Reminder.parseFromString("01-01-2001-2005");
        });
    }

    @Test
    public void testToString() {
        String pattern = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        assertEquals("id=null | Kerjain Tugas | remind date: " + date, reminder.toString());
    }

    @Test
    public void testToThePastException() {
        assertThrows(IllegalArgumentException.class, () -> {
            Reminder.parseFromString("01-01-2001");
        });
    }
}
