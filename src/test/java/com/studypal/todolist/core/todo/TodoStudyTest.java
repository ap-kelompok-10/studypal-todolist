package com.studypal.todolist.core.todo;

import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TodoStudyTest {
    private Class<?> todoStudyClass;
    private TodoStudy todoStudy;

    @BeforeEach
    public void setup() throws Exception {
        todoStudyClass = Class.forName("com.studypal.todolist.core.todo.TodoStudy");
        todoStudy = new TodoStudy("ID01", "Belajar Adpro", 2, LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
    }

    @Test
    public void testRename() {
        todoStudy.rename("Tugas Kelompok Adpro");
        assertEquals("Tugas Kelompok Adpro", todoStudy.getEntryName());
    }

    @Test
    public void testSetDateTime() {
        todoStudy.setDateTime(LocalDateTime.of(2020, 9, 5, 6, 40, 45));
        assertEquals(LocalDateTime.of(2020, 9, 5, 6, 40, 45), todoStudy.getStartTime());
    }

    @Test
    public void testChangeDuration() {
        todoStudy.changeDuration(4);
        assertEquals(4, todoStudy.getDuration());
    }

    @Test
    public void testGetEntryName() {
        assertEquals("Belajar Adpro", todoStudy.getEntryName());
    }

    @Test
    public void testGetDuration() {
        assertEquals(2, todoStudy.getDuration());
    }

    @Test
    public void testGetStartTime() {
        assertEquals(LocalDateTime.of(2020, 9, 10, 6, 40, 45), todoStudy.getStartTime());
    }

    @Test
    public void testGetFinishTime() {
        LocalDateTime finish = LocalDateTime.of(2020, 9, 10, 6,
                40, 45).plusHours(todoStudy.getDuration());
        assertEquals(finish, todoStudy.getFinishTime());
    }

    @Test
    public void testParameterlessConstructor() {
        todoStudy = new TodoStudy();
        assertNull(todoStudy.getEntryName());
    }
}
