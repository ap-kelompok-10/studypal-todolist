package com.studypal.todolist.core.todo;

import java.time.Duration;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TodoTaskTest {
    private Class<?> todoTaskClass;
    private TodoTask todoTask;

    @BeforeEach
    public void setup() throws Exception {
        todoTaskClass = Class.forName("com.studypal.todolist.core.todo.TodoTask");
        todoTask = new TodoTask("ID01", "Tutorial Adpro",
                "Advanced Programming", LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
    }

    @Test
    public void testRename() {
        todoTask.rename("Tugas Kelompok Adpro");
        assertEquals("Tugas Kelompok Adpro", todoTask.getEntryName());
    }

    @Test
    public void testSetDateTime() {
        todoTask.setDateTime(LocalDateTime.of(2020, 9, 5, 6, 40, 45));
        assertEquals(LocalDateTime.of(2020, 9, 5, 6, 40, 45), todoTask.getDueDate());
    }

    @Test
    public void testGetEntryName() {
        assertEquals("Tutorial Adpro", todoTask.getEntryName());
    }

    @Test
    public void testGetDueDate() {
        assertEquals(LocalDateTime.of(2020, 9, 10, 6, 40, 45), todoTask.getDueDate());
    }

    @Test
    public void testGetCourse() {
        assertEquals("Advanced Programming", todoTask.getCourse());
    }

    @Test
    public void testGetDuration() {
        long days = Duration.between(LocalDateTime.now(), todoTask.getDueDate()).toDays();
        assertEquals(days, todoTask.getDuration());
    }

    @Test
    public void testParameterlessConstructor() {
        todoTask = new TodoTask();
        assertNull(todoTask.getEntryName());
    }
}
