package com.studypal.todolist.core.todo;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TodoTest {
    private Class<?> todoClass;

    @BeforeEach
    public void setUp() throws Exception {
        todoClass = Class.forName("com.studypal.todolist.core.todo.Todo");
    }

    @Test
    public void testTodoIsAPublicInterface() {
        int classModifiers = todoClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testTodoHasGetEntryNameAbstractMethod() throws Exception {
        Method cast = todoClass.getDeclaredMethod("getEntryName");
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, cast.getParameterCount());
    }

    @Test
    public void testTodoHasRenameAbstractMethod() throws Exception {
        Method cast = todoClass.getDeclaredMethod("rename", String.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

    @Test
    public void testTodoHasSetDateTimeAbstractMethod() throws Exception {
        Method cast = todoClass.getDeclaredMethod("setDateTime", LocalDateTime.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, cast.getParameterCount());
    }

}
