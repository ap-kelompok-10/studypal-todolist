package com.studypal.todolist.core.tasker;

import com.studypal.todolist.core.todo.TodoTask;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static java.lang.Class.forName;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class OrganizerTest {
    private Class<?> organizerClass;
    private Organizer organizer;
    private TodoTask todoTask;

    @BeforeEach
    public void setup() throws Exception {
        todoTask = new TodoTask("ID01", "Tutorial Adpro",
                "Advanced Programming", LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        organizerClass = forName("com.studypal.todolist.core.tasker.Organizer");
        organizer = new Organizer("Tutorial Adpro", "Advanced Programming", LocalDateTime.of(2020, 9,
                10, 3, 20, 30), todoTask);
    }

    @Test
    public void testSetDuedate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        organizer.setDuedate(LocalDateTime.of(2020, 9,
                10, 3, 20, 30));
        assertEquals(LocalDateTime.of(2020, 9,
                10, 3, 20, 30).format(formatter), organizer.getDuedate().format(formatter));
    }

    @Test
    public void testGetEntryName() {
        assertEquals("Tutorial Adpro", organizer.getEntryName());
    }

    @Test
    public void testSetEntryName() {
        organizer.setEntryName("Tutorial Basdat");
        assertEquals("Tutorial Basdat", organizer.getEntryName());
    }


    @Test
    public void testGetDuedate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        assertEquals(LocalDateTime.of(2020, 9,
                10, 3, 20, 30).format(formatter), organizer.getDuedate().format(formatter));
    }

    @Test
    public void testGetCourse() {
        assertEquals("Advanced Programming", organizer.getCourse());
    }

    @Test
    public void testSetCourse() {
        organizer.setCourse("Basdat");
        assertEquals("Basdat", organizer.getCourse());
    }

    @Test
    public void testCompareTo() {
        Organizer dummy = new Organizer("Tutorial Adpro", "Advanced Programming", LocalDateTime.of(2020, 9,
                1, 3, 20, 30), todoTask);
        assertEquals(9, organizer.compareTo(dummy));
    }

    @Test
    public void testCompareToNullComparatorOther() {
        Organizer dummy = new Organizer("Tutorial Adpro", "Advanced Programming",null, todoTask);
        assertEquals(0, organizer.compareTo(dummy));
    }

    @Test
    public void testCompareToNullDate() {
        organizer.setDuedate(null);
        Organizer dummy = new Organizer("Tutorial Adpro", "Advanced Programming", LocalDateTime.of(2020, 9,
                1, 3, 20, 30), todoTask);
        assertEquals(0, organizer.compareTo(dummy));
    }
}