package com.studypal.todolist.controller;

import com.studypal.todolist.core.bot.Bot;
import com.studypal.todolist.core.todo.TodoStudy;
import com.studypal.todolist.core.todo.TodoTask;
import com.studypal.todolist.repository.BotRepository;
import com.studypal.todolist.repository.TodoStudyRepository;
import com.studypal.todolist.service.reminder.ReminderServiceImpl;
import com.studypal.todolist.service.todolist.TodolistServiceImpl;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.StickerMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;
import com.linecorp.bot.model.response.BotApiResponse;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.studypal.todolist.service.todolist.TodolistServiceImpl.taskHashMapMaker;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MainControllerTest {
    @Mock
    private LineMessagingClient lineMessagingClient;

    @Mock
    BotRepository botRepository;

    @Mock
    TodoStudyRepository todoStudyRepository;

    @Mock
    ReminderServiceImpl reminderService;

    @Mock
    TodolistServiceImpl todolistService;

    @Mock
    Bot dummyBot;

    @Mock
    private CompletableFuture<UserProfileResponse> cpuresp;

    @Mock
    private CompletableFuture<BotApiResponse> botresp;

    @InjectMocks
    private ReplyMessage replyMessage;

    String command;
    String userId;
    Bot bot;
    String replyToken;
    TextMessageContent message;


    @Spy
    @InjectMocks
    MainController mainController;

    @BeforeEach
    public void setUp() throws ExecutionException, InterruptedException {
        userId = "1";
        bot = dummyBot;
        replyToken = "replyToken";
        UserProfileResponse userProfileResponse = new UserProfileResponse(
                "Astrid", "1", "file://stub", "Hi");
        when(lineMessagingClient.getProfile("1")).thenReturn(cpuresp);
        lenient().when(lineMessagingClient.getProfile("1").get()).thenReturn(userProfileResponse);
    }

    @Test
    public void getUserDisplayNameTest() throws ExecutionException, InterruptedException {
        assertEquals(mainController.getUserDisplayName("1"), "Astrid");
    }

    @Test
    public void getUserDisplayNameTestNone() throws ExecutionException, InterruptedException {
        assertEquals(mainController.getUserDisplayName("13"), "None");
    }

    @Test
    public void testGetCommand() {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/help", "1");
        assertEquals(mainController.getCommand(messageEvent), "/help");
    }

    @Test
    public void testReplyChat() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        mainController.replyChat("replyToken", new TextMessage("Hi"));
        Mockito.verify(mainController).replyChat("replyToken", new TextMessage("Hi"));
    }

    @Test
    public void testReplySticker() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        mainController.replySticker("replyToken", new StickerMessage("11537", "52002735"));
        Mockito.verify(mainController).replySticker("replyToken", new StickerMessage("11537", "52002735"));
    }

    @Test
    public void testReplyChatNull() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(null);
        mainController.replyChat("replyToken", new TextMessage("Hi"));
        Mockito.verify(mainController).replyChat("replyToken", new TextMessage("Hi"));
    }

    @Test
    public void testReplyStickertNull() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(null);
        mainController.replySticker("replyToken", new StickerMessage("11537", "52002735"));
        Mockito.verify(mainController).replySticker("replyToken", new StickerMessage("11537", "52002735"));
    }

    @Test
    public void testReplyFlex() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        mainController.replyFlex("replyToken", new TextMessage("Hi"));
        Mockito.verify(mainController).replyFlex("replyToken", new TextMessage("Hi"));
    }

    @Test
    public void testReplyFlexNull() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(null);
        mainController.replyFlex("replyToken", new TextMessage("Hi"));
        Mockito.verify(mainController).replyFlex("replyToken", new TextMessage("Hi"));
    }

    @Test
    public void testHandleTextEvent() {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/help", "1");
        mainController.handleTextEvent(messageEvent);
        Mockito.verify(mainController).handleTextEvent(messageEvent);
    }

    @Test
    public void testHandleTextNotNull() {
        when(todolistService.getBotById(userId)).thenReturn(dummyBot);
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/help", "1");
        mainController.handleTextEvent(messageEvent);
        Mockito.verify(mainController).handleTextEvent(messageEvent);
    }

    @Test
    public void testHandleTextEventInvalidType() {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("/what", "1");
        mainController.handleTextEvent(messageEvent);
        Mockito.verify(mainController).handleTextEvent(messageEvent);
    }

    @Test
    public void testHandleTextEventInvalidTypeStart() {
        MessageEvent<TextMessageContent> messageEvent;
        messageEvent = EventTestUtil.createDummyTextMessage("what", "1");
        mainController.handleTextEvent(messageEvent);
        Mockito.verify(mainController).handleTextEvent(messageEvent);
    }

    @Test
    public void testHandleNoBotExist() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/custom-bot";
        message = new TextMessageContent("1", "/custom-bot / astrid / f / casual");
        mainController.handleNoBotExist(command, userId, replyToken, message);
        Mockito.verify(mainController).handleNoBotExist(command, userId, replyToken, message);
    }

    @Test
    public void testHandleNoBotExistInvalid() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/custom-bot";
        message = new TextMessageContent("1", "/custom-bot / astrid");
        mainController.handleNoBotExist(command, userId, replyToken, message);
        Mockito.verify(mainController).handleNoBotExist(command, userId, replyToken, message);
    }

    @Test
    public void testHandleNoBotExistUnknownCommand() {
        command = "/custom-bot-123";
        message = new TextMessageContent("1", "/custom-bot-123 / astrid / f / casual");
        mainController.handleNoBotExist(command, userId, replyToken, message);
        Mockito.verify(mainController).handleNoBotExist(command, userId, replyToken, message);
    }

    @Test
    public void testHelp() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/help";
        message = new TextMessageContent("1", "");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testUnknownCommand() {
        command = "/saya";
        message = new TextMessageContent("1", "/saya / sangat / tamvan");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testCustomBot() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/custom-bot";
        message = new TextMessageContent("1", "/custom-bot / astrid / f / casual / y");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testSticker() {
        lenient().when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/sticker";
        message = new TextMessageContent("1", "");
        lenient().when(bot.checkSticker()).thenReturn(true);
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testStickerFalse() {
        lenient().when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/sticker";
        message = new TextMessageContent("1", "");
        lenient().when(bot.checkSticker()).thenReturn(false);
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testCustomBotInvalid() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/custom-bot";
        message = new TextMessageContent("1", "");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testViewBot() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/view-bot";
        when(bot.getGender()).thenReturn("Male");
        when(bot.getSticker()).thenReturn("false");
        TextMessageContent message = new TextMessageContent("1", "");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testGetMotivation() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/get-motivation";
        message = new TextMessageContent("1", "");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testViewTodoTask() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/view-todotask";
        message = new TextMessageContent("1", "");
        TodoTask todoTask = new TodoTask("1", "Tutorial Adpro",
                "Advanced Programming", LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        List<TodoTask> tasks = new ArrayList<>();
        tasks.add(todoTask);
        when(todolistService.getTodoTasks("1")).thenReturn(taskHashMapMaker(tasks));
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testViewTodoTaskEmpty() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/view-todotask";
        message = new TextMessageContent("1", "");
        when(todolistService.getTodoTasks("1")).thenReturn(taskHashMapMaker(new ArrayList<TodoTask>()));
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testViewTodoStudyEmpty() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/view-todostudy";
        message = new TextMessageContent("1", "");
        when(todolistService.getTodoStudies("1")).thenReturn(new ArrayList<TodoStudy>());
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testViewTodoStudy() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/view-todostudy";
        message = new TextMessageContent("1", "");
        TodoStudy todoStudy = new TodoStudy("1", "Belajar Adpro", 2, LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        List<TodoStudy> studys = new ArrayList<>();
        studys.add(todoStudy);
        when(todolistService.getTodoStudies("1")).thenReturn(studys);
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testAddTodoTask() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/add-todotask";
        message = new TextMessageContent("1", "/add-todotask / test / basdat / 2020-05-11-11:55");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testAddTodoTaskInvalidDate() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/add-todotask";
        message = new TextMessageContent("1", "/add-todotask / test / basdat / 2020-05-11");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testAddTodoTaskAlreadyExist() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/add-todotask";
        message = new TextMessageContent("1", "/add-todotask / test / basdat / 2020-05-11-11:55");
        TodoTask todoTask = new TodoTask("1", "test",
                "Basdat", LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        when(todolistService.getTodoTaskByEntry("test")).thenReturn(todoTask);
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testAddTodoTaskInvalid() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/add-todotask";
        message = new TextMessageContent("1", "");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testAddTodoStudy() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/add-todostudy";
        message = new TextMessageContent("1", "/add-todostudy / test / 4 / 2020-05-11-11:55");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testAddTodoStudyInvalidDate() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/add-todostudy";
        message = new TextMessageContent("1", "/add-todostudy / test / 4 / 2020-05-11");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testAddTodoStudyInvalidDuration() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/add-todostudy";
        message = new TextMessageContent("1", "/add-todostudy / test / 4/");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testAddTodoStudyAlreadyExist() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/add-todostudy";
        message = new TextMessageContent("1", "/add-todostudy / test / 4 / 2020-05-11-11:55");
        TodoStudy todoStudy = new TodoStudy("1", "test", 2, LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        when(todolistService.getTodoStudyByEntry("test")).thenReturn(todoStudy);
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testAddTodoStudyInvalid() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/add-todostudy";
        message = new TextMessageContent("1", "");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testUpdateTodoTaskNotExist() {
        command = "/update-todotask";
        message = new TextMessageContent("1", "/update-todotask / test / basdat / 2020-05-11-11:55");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testUpdateTodoTask() {
        command = "/update-todotask";
        message = new TextMessageContent("1", "/update-todotask / test / basdat / 2020-05-11-11:55");
        TodoTask todoTask = new TodoTask("1", "test",
                "Basdat", LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        when(todolistService.getTodoTaskByEntry("test")).thenReturn(todoTask);
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testUpdateTodoTaskInvalidDate() {
        command = "/update-todotask";
        message = new TextMessageContent("1", "/update-todotask / test / basdat / 2020-05");
        TodoTask todoTask = new TodoTask("1", "test",
                "Basdat", LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        lenient().when(todolistService.getTodoTaskByEntry("test")).thenReturn(todoTask);
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testUpdateTodoTaskInvalid() {
        command = "/update-todotask";
        message = new TextMessageContent("1", "");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testUpdateTodoStudyNotExist() {
        command = "/update-todostudy";
        message = new TextMessageContent("1", "/update-todostudy / test / 4 / 2020-05-11-11:55");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testUpdateTodoStudy() {
        command = "/update-todostudy";
        message = new TextMessageContent("1", "/update-todostudy / test / 4 / 2020-05-11-11:55");
        TodoStudy todoStudy = new TodoStudy("1", "test", 2, LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        when(todolistService.getTodoStudyByEntry("test")).thenReturn(todoStudy);
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testUpdateTodoStudyInvalidDate() {
        command = "/update-todostudy";
        message = new TextMessageContent("1", "/update-todostudy / test / 4 / 2020-05-11");
        TodoStudy todoStudy = new TodoStudy("1", "test", 2, LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        lenient().when(todolistService.getTodoStudyByEntry("test")).thenReturn(todoStudy);
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    
    @Test
    public void testUpdateTodoStudyInvalidDuration() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/update-todostudy";
        message = new TextMessageContent("1", "/update-todostudy / test / 4/");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testUpdateTodoStudyInvalid() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/update-todostudy";
        message = new TextMessageContent("1", "");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testCheckTaskNotExist() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/check-task";
        message = new TextMessageContent("1", "/check-task / test");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testCheckTask() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/check-task";
        message = new TextMessageContent("1", "/check-task / test");
        TodoTask todoTask = new TodoTask("1", "test",
                "Basdat", LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        when(todolistService.getTodoTaskByEntry("test")).thenReturn(todoTask);
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testCheckTaskInvalid() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/check-task";
        message = new TextMessageContent("1", "");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testCheckStudyNotExist() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/check-study";
        message = new TextMessageContent("1", "/check-study / test");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testCheckStudy() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/check-study";
        message = new TextMessageContent("1", "/check-study / test");
        TodoStudy todoStudy = new TodoStudy("1", "test", 2, LocalDateTime.of(2020, 9,
                10, 6, 40, 45));
        when(todolistService.getTodoStudyByEntry("test")).thenReturn(todoStudy);
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testCheckStudyInvalid() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/check-study";
        message = new TextMessageContent("1", "");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testReminder() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/reminder";
        message = new TextMessageContent("1", "/reminder / 10-10-2020 / Test");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testReminderInvalid() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/reminder";
        message = new TextMessageContent("1", "");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testReminderManagerView() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/reminder-manager";
        message = new TextMessageContent("1", "/reminder-manager / view");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testReminderManagerDelete() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/reminder-manager";
        message = new TextMessageContent("1", "/reminder-manager / delete / 4");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testReminderManagerDeleteInvalid() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/reminder-manager";
        message = new TextMessageContent("1", "/reminder-manager / delete / Uhuy");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testReminderUnknownCommand() {
        command = "/reminder-manager";
        message = new TextMessageContent("1", "/reminder-manager / assasinate / 2");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }

    @Test
    public void testReminderManagerInvalid() {
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(botresp);
        command = "/reminder-manager";
        message = new TextMessageContent("1", "");
        mainController.handleBotExist(command, userId, bot, replyToken, message);
        Mockito.verify(mainController).handleBotExist(command, userId, bot, replyToken, message);
    }
}
