package com.studypal.todolist.controller;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.UserSource;
import java.time.Instant;
import org.springframework.stereotype.Component;

@Component
public class EventTestUtil {
    private EventTestUtil() {
    }

    public static MessageEvent<TextMessageContent> createDummyTextMessage(String text, String userId) {
        return new MessageEvent<>("replyToken", new UserSource(userId),
                new TextMessageContent("id", text),
                Instant.parse("2018-01-01T00:00:00.000Z"));
    }
}
