package com.studypal.todolist;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@EnableAutoConfiguration
@ExtendWith(SpringExtension.class)
class TodoListApplicationTest {

    @Test
    void contextLoads() {
        TodolistApplication.main(new String[]{});
    }

}
