package com.studypal.todolist.controller;

import com.studypal.todolist.core.bot.Bot;
import com.studypal.todolist.core.reminder.Reminder;
import com.studypal.todolist.core.todo.TodoStudy;
import com.studypal.todolist.core.todo.TodoTask;
import com.studypal.todolist.service.flex.BotProfileFlexMessage;
import com.studypal.todolist.service.flex.TodostudyFlexMessage;
import com.studypal.todolist.service.flex.TodotaskFlexMessage;
import com.studypal.todolist.service.message.Messages;
import com.studypal.todolist.service.reminder.ReminderServiceImpl;
import com.studypal.todolist.service.todolist.TodolistServiceImpl;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.StickerMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

@LineMessageHandler
public class MainController {
    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private TodolistServiceImpl todolistService;

    @Autowired
    private ReminderServiceImpl reminderService;

    @Autowired
    private Messages replies;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm");

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent) {
        String userId = messageEvent.getSource().getUserId();
        String command = getCommand(messageEvent);
        Bot bot = todolistService.getBotById(userId);
        String replyToken = messageEvent.getReplyToken();
        TextMessageContent message = messageEvent.getMessage();
        if (bot != null) {
            handleBotExist(command, userId, bot, replyToken, message);
        } else {
            if (command.equals("/custom-bot") | command.equals("/help")) {
                handleNoBotExist(command, userId, replyToken, message);
            } else {
                if (command.startsWith("/")) {
                    replyChat(replyToken, replies.BOT_NOT_EXIST);
                }
            }
        }
    }

    public void handleNoBotExist(String command, String userId,
                                 String replyToken, TextMessageContent message) {
        String[] messages = message.getText().split(" / ");
        String displayName = getUserDisplayName(userId);
        switch (command) {
            case "/help":
                replyChat(replyToken, replies.HELP);
                break;
            case "/custom-bot":
                try {
                    createNewBot(messages, userId, displayName);
                    replyChat(replyToken, replies.CREATE_BOT);
                } catch (IndexOutOfBoundsException e) {
                    replyChat(replyToken, replies.INVALID);
                }
                break;
        }
    }

    public void createNewBot(String[] messages, String userId, String displayName) {
        String botName = messages[1];
        String botGender = messages[2];
        String botBehaviour = messages[3];
        Bot newBot = new Bot(botName, botGender, botBehaviour,
                userId, displayName);
        todolistService.addBot(newBot);
    }

    public void handleBotExist(String command, String userId, Bot bot,
                               String replyToken, TextMessageContent message) {
        String[] messages = message.getText().split(" / ");
        switch (command) {
            case "/help":
                replyChat(replyToken, replies.HELP);
                break;
            case "/custom-bot":
                try {
                    customBot(messages, bot);
                    replyChat(replyToken, replies.CUSTOMIZE_BOT);
                } catch (IndexOutOfBoundsException e) {
                    replyChat(replyToken, replies.INVALID);
                }
                break;
            case "/view-bot":
                BotProfileFlexMessage profileFlexMessage = new BotProfileFlexMessage(
                        bot.getName(),
                        bot.getGender(),
                        bot.getMotivationType(),
                        bot.getOwnerName(),
                        bot.getSticker()
                );
                this.replyFlex(replyToken, profileFlexMessage.get());
                break;
            case "/get-motivation":
                String motivation = bot.getMotivation();
                replyChat(replyToken, replies.getMotivation(motivation));
                break;
            case "/sticker":
                if (bot.checkSticker()) {
                    replySticker(replyToken, replies.STICKER_OK);
                } else {
                    replySticker(replyToken, replies.STICKER_ON_FIRE);
                }
                break;
            case "/view-todotask":
                viewTodoTask(userId, replyToken);
                break;
            case "/view-todostudy":
                viewTodoStudy(userId, replyToken);
                break;
            case "/add-todotask":
                try {
                    addTodoTask(messages, userId, replyToken, bot);
                } catch (IndexOutOfBoundsException e) {
                    replyChat(replyToken, replies.INVALID);
                }
                break;
            case "/add-todostudy":
                try {
                    addTodoStudy(messages, userId, replyToken, bot);
                } catch (IndexOutOfBoundsException e) {
                    replyChat(replyToken, replies.INVALID);
                } catch (NumberFormatException e) {
                    replyChat(replyToken, replies.INVALID_DURATION);
                }
                break;
            case "/update-todotask":
                try {
                    updateTodoTask(messages, userId, replyToken, bot);
                } catch (IndexOutOfBoundsException e) {
                    replyChat(replyToken, replies.INVALID);
                }
                break;
            case "/update-todostudy":
                try {
                    updateTodoStudy(messages, userId, replyToken, bot);
                } catch (IndexOutOfBoundsException e) {
                    replyChat(replyToken, replies.INVALID);
                } catch (NumberFormatException e) {
                    replyChat(replyToken, replies.INVALID_DURATION);
                }
                break;
            case "/check-task":
                try {
                    checkTask(messages, replyToken, bot);
                } catch (IndexOutOfBoundsException e) {
                    replyChat(replyToken, replies.INVALID);
                }
                break;
            case "/check-study":
                try {
                    checkStudy(messages, replyToken, bot);
                } catch (IndexOutOfBoundsException e) {
                    replyChat(replyToken, replies.INVALID);
                }
                break;
            case "/reminder":
                try {
                    addReminder(messages, userId);
                    replyChat(replyToken, replies.ADD_REMINDER);
                } catch (IndexOutOfBoundsException | IllegalArgumentException e) {
                    replyChat(replyToken, replies.INVALID);
                }
                break;
            case "/reminder-manager":
                try {
                    reminderManager(messages, userId, replyToken);
                } catch (IndexOutOfBoundsException | IllegalArgumentException e) {
                    replyChat(replyToken, replies.REMIND_MANAGE_HELPER);
                }
                break;
        }
    }

    public void customBot(String[] messages, Bot bot) {
        String botName = messages[1];
        String botGender = messages[2];
        String botBehaviour = messages[3];
        String stickerType = messages[4];
        bot.changeName(botName);
        bot.changeGender(botGender);
        bot.changeMotivationType(botBehaviour);
        bot.changeSticker(stickerType);
        todolistService.addBot(bot);
    }

    public void viewTodoTask(String userId, String replyToken) {
        HashMap<String, List<TodoTask>> todoTasks = todolistService.getTodoTasks(userId);
        if (todoTasks.isEmpty()) {
            this.replyFlex(replyToken, replies.NO_TASK_EXIST);
        } else {
            TodotaskFlexMessage todotaskFlexMessage = new TodotaskFlexMessage(todoTasks);
            this.replyFlex(replyToken, todotaskFlexMessage.get());
        }
    }

    public void viewTodoStudy(String userId, String replyToken) {
        List<TodoStudy> todoStudies = todolistService.getTodoStudies(userId);
        if (todoStudies.isEmpty()) {
            this.replyFlex(replyToken, replies.NO_STUDY_EXIST);
        } else {
            TodostudyFlexMessage todostudyFlexMessage = new TodostudyFlexMessage(todoStudies);
            this.replyFlex(replyToken, todostudyFlexMessage.get());
        }
    }

    public void addTodoTask(String[] messages, String userId, String replyToken, Bot bot) {
        String entryName = messages[1];
        String course = messages[2].toUpperCase();
        String due = messages[3];
        try {
            LocalDateTime dueDate = LocalDateTime.parse(due, formatter);
            TodoTask task = new TodoTask(userId, entryName, course, dueDate);
            if (todolistService.getTodoTaskByEntry(entryName) != null) {
                replyChat(replyToken, replies.TASK_VIOLATE_CONSTRAINT);
            } else {
                todolistService.addTodoTask(task);
                replyChat(replyToken, replies.ADD_TODO_TASK);
            }
        } catch (DateTimeParseException e) {
            replyChat(replyToken, replies.INVALID_DATE);
        }
    }

    public void addTodoStudy(String[] messages, String userId, String replyToken, Bot bot) {
        String entryName = messages[1];
        String durationStr = messages[2];
        int duration = Integer.parseInt(durationStr);
        String start = messages[3];
        try {
            LocalDateTime startTime = LocalDateTime.parse(start, formatter);
            TodoStudy study = new TodoStudy(userId, entryName, duration, startTime);
            if (todolistService.getTodoStudyByEntry(entryName) != null) {
                replyChat(replyToken, replies.STUDY_VIOLATE_CONSTRAINT);
            } else {
                todolistService.addTodoStudy(study);
                replyChat(replyToken, replies.ADD_TODO_STUDY);
            }
        } catch (DateTimeParseException e) {
            replyChat(replyToken, replies.INVALID_DATE);
        }
    }

    public void updateTodoTask(String[] messages, String userId, String replyToken, Bot bot) {
        String entryName = messages[1];
        String course = messages[2].toUpperCase();
        String due = messages[3];
        try {
            LocalDateTime dueDate = LocalDateTime.parse(due, formatter);
            TodoTask task = new TodoTask(userId, entryName, course, dueDate);
            if (todolistService.getTodoTaskByEntry(entryName) != null) {
                todolistService.addTodoTask(task);
                replyChat(replyToken, replies.UPDATE_TODO_TASK);
            } else {
                replyChat(replyToken, replies.TASK_NOT_EXIST);
            }
        } catch (DateTimeParseException e) {
            replyChat(replyToken, replies.INVALID_DATE);
        }
    }

    public void updateTodoStudy(String[] messages, String userId, String replyToken, Bot bot) {
        String entryName = messages[1];
        String durationStr = messages[2];
        int duration = Integer.parseInt(durationStr);
        String start = messages[3];
        try {
            LocalDateTime startTime = LocalDateTime.parse(start, formatter);
            TodoStudy study = new TodoStudy(userId, entryName, duration, startTime);
            if (todolistService.getTodoStudyByEntry(entryName) != null) {
                todolistService.addTodoStudy(study);
                replyChat(replyToken, replies.UPDATE_TODO_STUDY);
            } else {
                replyChat(replyToken, replies.STUDY_NOT_EXIST);
            }
        } catch (DateTimeParseException e) {
            replyChat(replyToken, replies.INVALID_DATE);
        }
    }

    public void checkTask(String[] messages, String replyToken, Bot bot) {
        String taskName = messages[1];
        if (todolistService.getTodoTaskByEntry(taskName) != null) {
            todolistService.checklistTask(taskName);
            replyChat(replyToken, replies.CHECKLIST_TASK);
        } else {
            replyChat(replyToken, replies.TASK_NOT_EXIST);
        }
    }

    public void checkStudy(String[] messages, String replyToken, Bot bot) {
        String studyName = messages[1];
        if (todolistService.getTodoStudyByEntry(studyName) != null) {
            todolistService.checklistStudy(studyName);
            replyChat(replyToken, replies.CHECKLIST_STUDY);
        } else {
            replyChat(replyToken, replies.STUDY_NOT_EXIST);
        }
    }

    public void reminderManager(String[] messages, String userId, String replyToken) {
        String manageCommand = messages[1];
        if (manageCommand.equalsIgnoreCase("view")) {
            List<Reminder> reminderList = reminderService.getReminderByOwnerId(userId);
            replyChat(replyToken, replies.generateReminderResponse(reminderList));
        } else if (manageCommand.equalsIgnoreCase("delete")) {
            try {
                int toDeleteId = Integer.parseInt(messages [2]);
                reminderService.deleteReminderById(userId, toDeleteId);
                replyChat(replyToken, replies.DELETE_REMINDER);
            } catch (NumberFormatException e) {
                replyChat(replyToken, replies.INVALID);
            }
        }
    }

    public void addReminder(String[] messages, String userId) {
        String remindDate = messages[1];
        String description = messages[2];
        Date remindDateFormatted = Reminder.parseFromString(remindDate);
        Reminder reminder = new Reminder(userId, remindDateFormatted, description);
        reminderService.addReminder(reminder);
    }

    public String getUserDisplayName(String userId) {
        try {
            return lineMessagingClient
                    .getProfile(userId)
                    .get()
                    .getDisplayName();
        } catch (ExecutionException | InterruptedException | NullPointerException e) {
            return "None";
        }
    }

    public boolean replyChat(String replyToken, TextMessage reply) {
        try {
            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, reply))
                    .get();
            return true;
        } catch (InterruptedException | ExecutionException | NullPointerException e) {
            System.out.println("Error Ocurred");
            return false;
        }
    }

    public String getCommand(MessageEvent<TextMessageContent> event) {
        return event
                .getMessage()
                .getText()
                .split(" / ")[0]
                .toLowerCase();
    }

    public boolean replyFlex(String replyToken, Message messages) {
        try {
            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, messages))
                    .get();
            return true;
        } catch (InterruptedException | ExecutionException | NullPointerException e) {
            System.out.println("Error Ocurred");
            return false;
        }
    }

    public boolean replySticker(String replyToken, StickerMessage reply) {
        try {
            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, reply))
                    .get();
            return true;
        } catch (InterruptedException | ExecutionException | NullPointerException e) {
            System.out.println("Error Ocurred");
            return false;
        }
    }
}
