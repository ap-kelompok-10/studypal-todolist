package com.studypal.todolist.service.reminder;

import com.studypal.todolist.core.reminder.Reminder;
import com.studypal.todolist.repository.ReminderRepository;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReminderServiceImpl implements ReminderService {
    @Autowired
    private ReminderRepository reminderRepository;

    public ReminderServiceImpl() {
    }

    @Override
    public Reminder addReminder(Reminder reminder) {
        return reminderRepository.save(reminder);
    }

    @Override
    public List<Reminder> getReminderByDate(Date date) {
        return reminderRepository.findByDate(date);
    }

    @Override
    public List<Reminder> getReminderByOwnerId(String ownerId) {
        return reminderRepository.findByOwnerId(ownerId);
    }

    @Override
    public void deleteReminderById(String ownerId, int id) {
        reminderRepository.deleteByIdWithOwnerValidation(ownerId, id);
    }
}
