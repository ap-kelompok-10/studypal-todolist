package com.studypal.todolist.service.reminder;

import com.studypal.todolist.core.reminder.Reminder;

import java.util.Date;
import java.util.List;

public interface ReminderService {
    Reminder addReminder(Reminder reminder);

    List<Reminder> getReminderByDate(Date date);

    List<Reminder> getReminderByOwnerId(String ownerId);

    void deleteReminderById(String ownerId, int id);
}
