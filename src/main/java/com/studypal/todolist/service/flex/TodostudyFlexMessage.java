package com.studypal.todolist.service.flex;

import com.studypal.todolist.core.todo.TodoStudy;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Filler;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class TodostudyFlexMessage implements Supplier<FlexMessage> {
    private List<TodoStudy> todoStudies;

    public TodostudyFlexMessage(List<TodoStudy> todoStudies) {
        this.todoStudies = todoStudies;
    }

    @Override
    public FlexMessage get() {
        final Box headerBlock = headerBlock();
        final Box bodyBlock = bodyBlock();
        final Bubble bubble = Bubble.builder()
                .header(headerBlock)
                .body(bodyBlock)
                .build();
        return new FlexMessage("View Todo Tasks", bubble);
    }

    public Box headerBlock() {
        final Text title = Text.builder()
                .text("TO DO STUDY")
                .align(FlexAlign.CENTER)
                .size(FlexFontSize.XXL)
                .weight(Text.TextWeight.BOLD)
                .color("#202046")
                .build();
        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(title))
                .build();
    }

    public Box bodyBlock() {
        List<FlexComponent> content = new ArrayList<>();
        for (TodoStudy study : todoStudies) {
            final Box entryBlock = entryBlock(study.getEntryName(),
                    study.getStartTime().toLocalTime().toString());
            content.add(entryBlock);
            final Filler filler = new Filler();
            final Box paddingBottom = Box.builder()
                    .layout(FlexLayout.VERTICAL)
                    .contents(asList(filler))
                    .margin(FlexMarginSize.MD)
                    .build();
            content.add(paddingBottom);
        }
        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.MD)
                .contents(content)
                .build();
    }

    public Box entryBlock(String task, String deadline) {
        final Text taskName = Text.builder()
                .text(task)
                .weight(Text.TextWeight.REGULAR)
                .margin(FlexMarginSize.SM)
                .flex(0)
                .build();
        final Text taskDue = Text.builder()
                .text(deadline)
                .weight(Text.TextWeight.REGULAR)
                .margin(FlexMarginSize.SM)
                .size(FlexFontSize.SM)
                .align(FlexAlign.END)
                .color("#aaaaaa")
                .build();
        return Box.builder()
                .layout(FlexLayout.BASELINE)
                .contents(asList(taskName, taskDue))
                .build();
    }
}


