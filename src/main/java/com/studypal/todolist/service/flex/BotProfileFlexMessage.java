package com.studypal.todolist.service.flex;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Spacer;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class BotProfileFlexMessage implements Supplier<FlexMessage> {
    private String botName;
    private String botGender;
    private String botBehaviour;
    private String ownerName;
    private String botSticker;
    private String imageLink;

    public BotProfileFlexMessage(String name, String gender, String behaviour, String ownerName, String sticker) {
        this.botName = name;
        this.botGender = gender;
        this.botBehaviour = behaviour;
        this.ownerName = ownerName;
        if (sticker.equalsIgnoreCase("true")) {
            this.botSticker = "Ok";
        } else {
            this.botSticker = "Fire";
        }
        if (botGender.equalsIgnoreCase("male")) {
            imageLink = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/18486534/aakk.jpg?width=64";
        } else {
            imageLink = "https://i.ibb.co/vJH5hR3/Screen-Shot-2020-05-15-at-00-53-39.png";
        }
    }

    @Override
    public FlexMessage get() {
        final Image heroBlock =
                Image.builder()
                        .url(imageLink)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .aspectRatio(Image.ImageAspectRatio.R20TO13)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build();
        final Box bodyBlock = bodyBlock();
        final Bubble bubble = Bubble.builder()
                .hero(heroBlock)
                .body(bodyBlock)
                .build();
        return new FlexMessage("Bot Profile", bubble);
    }

    private Box bodyBlock() {
        final Text title = Text.builder()
                .text("Bot Profile")
                .size(FlexFontSize.XL)
                .weight(Text.TextWeight.BOLD)
                .margin(FlexMarginSize.XL)
                .build();
        final Box spaceBlock = spaceBlock();
        final Box nameBlock = innerBlock("Name", botName);
        final Box genderBlock = innerBlock("Gender", botGender);
        final Box behaviourBlock = innerBlock("Behaviour", botBehaviour);
        final Box stickerBlock = innerBlock("Sticker", botSticker);
        final Box ownerBlock = innerBlock("Creator", ownerName);
        final Box footer = footerBlock();
        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.MD)
                .contents(asList(title, spaceBlock, nameBlock, genderBlock,
                        behaviourBlock, stickerBlock, ownerBlock, footer))
                .build();
    }

    private Box spaceBlock() {
        final Spacer spacer = Spacer.builder()
                .size(FlexMarginSize.XS)
                .build();
        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.MD)
                .contents(asList(spacer))
                .build();
    }

    private Box innerBlock(String label, String value) {
        final Text nameLabel = Text.builder()
                .text(label)
                .weight(Text.TextWeight.REGULAR)
                .margin(FlexMarginSize.SM)
                .flex(0)
                .color("#aaaaaa")
                .build();
        final Text name = Text.builder()
                .text(value)
                .size(FlexFontSize.Md)
                .align(FlexAlign.END)
                .weight(Text.TextWeight.REGULAR)
                .build();
        return Box.builder()
                .layout(FlexLayout.BASELINE)
                .spacing(FlexMarginSize.MD)
                .contents(asList(nameLabel, name))
                .build();
    }

    private Box footerBlock() {
        final Spacer spacer = Spacer.builder()
                .size(FlexMarginSize.XXL)
                .build();
        final Button button = Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .color("#202046")
                .action(new MessageAction("Start Chat", "/help"))
                .build();
        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(spacer, button))
                .build();
    }
}


