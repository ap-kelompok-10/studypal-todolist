package com.studypal.todolist.service.message;

import com.studypal.todolist.core.reminder.Reminder;

import com.linecorp.bot.model.message.StickerMessage;
import com.linecorp.bot.model.message.TextMessage;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class Messages {
    public static final TextMessage HELP = new TextMessage(
            "Welcome to StudyPal!\nBelow are the list of thing you can do:\n"
                    + "-> Customize bot : '/custom-bot / [name] / [gender: m/f] /"
                    + " [Behaviour: tough/casual] / [sticker: ok/fire]'\n"
                    + "-> View bot profile : '/view-bot'\n"
                    + "-> Get motivational quote : '/get-motivation'\n"
                    + "-> View todo tasks : '/view-todotask'\n"
                    + "-> View todo study : '/view-todostudy'\n"
                    + "-> Add todo task : '/add-todotask / [task_name] / [course_name] / "
                    + "[duedate : yyyy-MM-dd-HH:mm]'\n"
                    + "-> Add todo study : '/add-todostudy / [study_name] / [duration] / "
                    + "[start_time : yyyy-MM-dd-HH:mm]'\n"
                    + "-> Update todo task : '/update-todotask / [task_name] / [course_name] / "
                    + "[duedate : yyyy-MM-dd-HH:mm]'\n"
                    + "-> Update todo study : '/update-todostudy / [study_name] / [duration] / "
                    + "[start_time : yyyy-MM-dd-HH:mm]'\n"
                    + "-> Checklist/remove task : '/check-task / [todo_name]'\n"
                    + "-> Checklist/remove study : '/check-study / [todo_name]'\n"
                    + "-> Set reminder : '/reminder / [date : dd-MM-yyyy] / [description]'\n"
                    + "-> Manage reminder : '/reminder-manager / [view/delete]'\n"
                    + "-> Sticker : '/sticker'\n");

    public static final TextMessage ADD_TODO_TASK = new TextMessage(
            "Todo task registered.\nView your todo list by typing '/view-todotask'");

    public static final TextMessage UPDATE_TODO_TASK = new TextMessage(
            "Todo task updated.\nView your todo list by typing '/view-todotask'");

    public static final TextMessage ADD_TODO_STUDY = new TextMessage(
            "Todo study registered.\nView your todo list by typing '/view-todostudy'");

    public static final TextMessage UPDATE_TODO_STUDY = new TextMessage(
            "Todo study updated.\nView your todo list by typing '/view-todostudy'");

    public static final TextMessage CHECKLIST_TASK = new TextMessage(
            "Todo task removed successfully.");

    public static final TextMessage CHECKLIST_STUDY = new TextMessage(
            "Todo study removed successfully.");

    public static final TextMessage TASK_NOT_EXIST = new TextMessage(
            "The todo task name you entered does not exist.");

    public static final TextMessage NO_TASK_EXIST = new TextMessage(
            "You have no todo task registered. Type '/help' for more.");

    public static final TextMessage NO_STUDY_EXIST = new TextMessage(
            "You have no todo study registered. Type '/help' for more.");

    public static final TextMessage STUDY_NOT_EXIST = new TextMessage(
            "The todo study name you entered does not exist.");

    public static final TextMessage CREATE_BOT = new TextMessage(
            "Bot created successfully.");

    public static final TextMessage CUSTOMIZE_BOT = new TextMessage(
            "Bot attribute customized successfully.");

    public static final StickerMessage STICKER_OK = new StickerMessage(
            "11537", "52002735");
            
    public static final StickerMessage STICKER_ON_FIRE = new StickerMessage(
            "11537", "52002767");

    public static final TextMessage BOT_NOT_EXIST = new TextMessage(
            "You have no Bot initialized. Initialize with '/custom-bot [name] [gender: m/f] "
                    + "[Behaviour: formal/casual]'");

    public static final TextMessage ADD_REMINDER = new TextMessage(
            "Reminder created successfully.");

    public static final TextMessage TASK_VIOLATE_CONSTRAINT = new TextMessage(
            "Todo task with that entry name already exist.");

    public static final TextMessage STUDY_VIOLATE_CONSTRAINT = new TextMessage(
            "Todo study with that entry name already exist.");

    public static final TextMessage DELETE_REMINDER = new TextMessage(
            "Reminder (if exist) deleted successfully.");

    public static final TextMessage INVALID = new TextMessage(
            "Your input is invalid, try again.");

    public static final TextMessage NO_STICKER = new TextMessage(
            "You have not yet set any default sticker.");

    public static final TextMessage INVALID_DATE = new TextMessage(
            "The date you entered is invalid, try again.");

    public static final TextMessage INVALID_DURATION = new TextMessage(
            "The duration you entered is invalid, try again.");

    public  static final TextMessage REMIND_MANAGE_HELPER = new TextMessage(
            "you can view or delete your reminder:\n"
                + "-> list all upcoming reminder : /reminder-manager / view\n"
                + "-> delete upcoming manager: /reminder-manager / delete / [id]\n");

    public static final TextMessage getMotivation(String motivationType) {
        return new TextMessage(motivationType);
    }

    public static TextMessage generateReminderResponse(List<Reminder> fetchedReminder) {
        if (fetchedReminder.isEmpty()) {
            return new TextMessage("you have no reminder.");
        }
        String generatedMessage = "list of reminder:";
        for (Reminder reminder : fetchedReminder) {
            generatedMessage += '\n';
            generatedMessage += reminder;
        }
        return new TextMessage(generatedMessage);
    }
}
