package com.studypal.todolist.service.todolist;

import com.studypal.todolist.core.bot.Bot;
import com.studypal.todolist.core.todo.TodoStudy;
import com.studypal.todolist.core.todo.TodoTask;
import com.studypal.todolist.repository.BotRepository;
import com.studypal.todolist.repository.TodoStudyRepository;
import com.studypal.todolist.repository.TodoTaskRepository;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodolistServiceImpl implements TodolistService {

    @Autowired
    private TodoTaskRepository todoTaskRepository;

    @Autowired
    private TodoStudyRepository todoStudyRepository;

    @Autowired
    private BotRepository botRepository;

    public TodolistServiceImpl() {
    }

    @Override
    public Bot getBotById(String userId) {
        return botRepository.findBot(userId);
    }

    @Override
    public Bot addBot(Bot bot) {
        return botRepository.save(bot);
    }

    @Override
    public TodoTask addTodoTask(TodoTask task) {
        return todoTaskRepository.save(task);
    }

    @Override
    public TodoStudy addTodoStudy(TodoStudy study) {
        return todoStudyRepository.save(study);
    }

    @Override
    public TodoTask getTodoTaskByEntry(String entryName) {
        return todoTaskRepository.findTask(entryName);
    }

    @Override
    public TodoStudy getTodoStudyByEntry(String entryName) {
        return todoStudyRepository.findStudy(entryName);
    }

    @Override
    public HashMap<String, List<TodoTask>> getTodoTasks(String ownerId) {
        List<TodoTask> tasks = todoTaskRepository.fetchTasks(ownerId);
        return taskHashMapMaker(tasks);
    }

    public static HashMap<String, List<TodoTask>> taskHashMapMaker(List<TodoTask> tasks) {
        HashMap<String, List<TodoTask>> taskMap = new HashMap<>();
        for (TodoTask task : tasks) {
            String course = task.getCourse();
            if (taskMap.containsKey(course)) {
                taskMap.get(course).add(task);
            } else {
                List<TodoTask> taskList = new ArrayList<>();
                taskList.add(task);
                taskMap.put(course, taskList);
            }
        }
        return taskMap;
    }

    @Override
    public List<TodoStudy> getTodoStudies(String ownerId) {
        return todoStudyRepository.fetchStudies(ownerId);
    }

    @Override
    public void checklistTask(String task) {
        todoTaskRepository.deleteTask(task);
    }

    @Override
    public void checklistStudy(String study) {
        todoStudyRepository.deleteStudy(study);
    }

    @Override
    public long getRemainingTime(TodoTask task) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime dueDate = task.getDueDate();
        return now.until(dueDate, ChronoUnit.MINUTES);
    }
}

