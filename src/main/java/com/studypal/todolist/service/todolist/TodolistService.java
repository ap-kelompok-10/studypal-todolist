package com.studypal.todolist.service.todolist;

import com.studypal.todolist.core.bot.Bot;
import com.studypal.todolist.core.todo.Todo;
import com.studypal.todolist.core.todo.TodoStudy;
import com.studypal.todolist.core.todo.TodoTask;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public interface TodolistService {
    public TodoTask addTodoTask(TodoTask task);

    public TodoStudy addTodoStudy(TodoStudy study);

    public void checklistTask(String task);

    public void checklistStudy(String study);

    public long getRemainingTime(TodoTask task);

    public HashMap<String, List<TodoTask>> getTodoTasks(String ownerId);

    public List<TodoStudy> getTodoStudies(String ownerId);

    public Bot getBotById(String userId);

    public Bot addBot(Bot bot);

    public TodoTask getTodoTaskByEntry(String entryName);

    public TodoStudy getTodoStudyByEntry(String entryName);
}
