package com.studypal.todolist.core.todo;

import java.time.LocalDateTime;

public interface Todo {
    public void rename(String entryName);

    public void setDateTime(LocalDateTime date);

    public String getEntryName();
}
