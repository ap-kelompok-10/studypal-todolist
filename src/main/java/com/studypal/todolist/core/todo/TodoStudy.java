package com.studypal.todolist.core.todo;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "todo_study")
public class TodoStudy implements Todo {
    @Id
    @Column(name = "entry_name", nullable = false, unique = true)
    private String studyName;

    @Column(name = "duration", nullable = false)
    private int duration;

    @Column(name = "finish_time", nullable = false)
    private LocalDateTime finishTime;

    @Column(name = "start_time", nullable = false)
    private LocalDateTime startTime;

    @Column(name = "owner_id", nullable = false)
    private String ownerId;

    public TodoStudy() {
    }

    public TodoStudy(String ownerId, String studyName, int duration, LocalDateTime startTime) {
        this.ownerId = ownerId;
        rename(studyName);
        this.duration = duration;
        setDateTime(startTime);
    }

    @Override
    public void rename(String studyName) {
        this.studyName = studyName;
    }

    @Override
    public void setDateTime(LocalDateTime startTime) {
        this.startTime = startTime;
        this.finishTime = startTime.plusHours(getDuration());
    }

    public void changeDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public String getEntryName() {
        return this.studyName;
    }

    public int getDuration() {
        return this.duration;
    }

    public LocalDateTime getStartTime() {
        return this.startTime;
    }

    public LocalDateTime getFinishTime() {
        return this.finishTime;
    }
}
