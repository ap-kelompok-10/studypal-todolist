package com.studypal.todolist.core.todo;

import java.time.Duration;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "todo_task")
public class TodoTask implements Todo {
    @Id
    @Column(name = "entry_name", nullable = false, unique = true)
    private String taskName;

    @Column(name = "course", nullable = false)
    private String course;

    @Column(name = "duedate", nullable = false)
    private LocalDateTime dueDate;

    @Column(name = "owner_id", nullable = false)
    private String ownerId;

    public TodoTask() {
    }

    public TodoTask(String ownerId, String taskName, String course, LocalDateTime dueDate) {
        this.ownerId = ownerId;
        rename(taskName);
        this.course = course;
        setDateTime(dueDate);
    }

    @Override
    public void rename(String taskName) {
        this.taskName = taskName;
    }

    @Override
    public void setDateTime(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    public String getEntryName() {
        return this.taskName;
    }

    public LocalDateTime getDueDate() {
        return this.dueDate;
    }

    public long getDuration() {
        return Duration.between(LocalDateTime.now(), dueDate).toDays();
    }

    public String getCourse() {
        return this.course;
    }
}
