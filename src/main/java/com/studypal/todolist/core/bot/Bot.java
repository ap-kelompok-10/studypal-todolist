package com.studypal.todolist.core.bot;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bot")
public class Bot {
    @Id
    @Column(name = "owner_id", nullable = false, unique = true)
    private String ownerId;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "gender", nullable = false)
    private String gender;

    @Column(name = "motivation_type", nullable = false)
    private String motivationType;

    @Column(name = "owner_name", nullable = false)
    private String ownerName;

    @Column(name = "sticker", nullable = false)
    private String sticker;

    public Bot() {
    }

    public Bot(String name, String gender, String motivation, String ownerId, String ownerName) {
        this.ownerId = ownerId;
        this.ownerName = ownerName;
        changeName(name);
        changeMotivationType(motivation);
        changeGender(gender);
        changeSticker("fire");
    }

    public void changeName(String name) {
        this.name = name;
    }

    public void changeSticker(String sticker) {
        if (sticker.equalsIgnoreCase("ok")) {
            this.sticker = "true";
        } else if (sticker.equalsIgnoreCase("fire")) {
            this.sticker = "false";
        }
    }

    public void changeGender(String gender) {
        if (gender.equalsIgnoreCase("m")) {
            this.gender = "Male";
        } else if (gender.equalsIgnoreCase("f")) {
            this.gender = "Female";
        }
    }

    public void changeMotivationType(String behaviour) {
        if (behaviour.equalsIgnoreCase("casual")
                || behaviour.equalsIgnoreCase("tough")) {
            this.motivationType = behaviour;
        }
    }

    public String getName() {
        return this.name;
    }

    public String getSticker() {
        return this.sticker;
    }

    public String getGender() {
        return this.gender;
    }

    public String getMotivation() {
        if (motivationType.equalsIgnoreCase("casual")) {
            Casual motivation = new Casual();
            return motivation.getMotivation();
        } else {
            Tough motivation = new Tough();
            return motivation.getMotivation();
        }
    }

    public String getMotivationType() {
        return this.motivationType;
    }

    public String getOwnerName() {
        return this.ownerName;
    }

    public boolean checkSticker() {
        if (sticker.equalsIgnoreCase("true")) {
            return true;
        } else {
            return false;
        }
    }
}
