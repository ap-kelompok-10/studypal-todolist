package com.studypal.todolist.core.bot;

import java.util.ArrayList;
import java.util.Random;

public class Casual implements Motivation {
    private ArrayList<String> messages;
    private Random randomGenerator;

    public Casual() {
        randomGenerator = new Random();
        messages = new ArrayList<>();
        messages.add("“Your positive action combined with positive thinking "
                + "results in success.“ \n– Shiv Khera");
        messages.add("“Success is the sum of small efforts, repeated day in "
                + "and day out.“ \n– Robert Collier");
        messages.add("“I’ve failed over and over and over again in my life. "
                + "And that is why I succeed.“ \n– Michael Jordan");
        messages.add("“An investment in knowledge pays the best interest.”\n- "
                + "Benjamin Franklin");
        messages.add("“If you work hard enough and assert yourself, and use "
                + "your mind and imagination, you can shape the world to your "
                + "desires.” \n– Malcolm Gladwell");
    }

    @Override
    public String getMotivation() {
        int index = randomGenerator.nextInt(messages.size());
        return messages.get(index);
    }
}