package com.studypal.todolist.core.bot;

import java.util.ArrayList;
import java.util.Random;

public class Tough implements Motivation {
    private ArrayList<String> messages;
    private Random randomGenerator;

    public Tough() {
        randomGenerator = new Random();
        messages = new ArrayList<>();
        messages.add("“Successful people begin where failures leave "
                + "off. Never settle for ‘just getting the job done.’ "
                + "Excel!” \n– Tom Hopkins");
        messages.add("“Talent is cheaper than table salt. What separates "
                + "the talented individual from the successful one is a lot of "
                + "hard work.” \n– Stephen King");
        messages.add("“Procrastination is opportunity’s assassin.“ "
                + "\n– Victor Kiam");
        messages.add("“Ninety-nine percent of the failures come from "
                + "people who have the habit of making excuses.“ \n– George Washington");
        messages.add("“Don’t wish it were easier, wish you were "
                + "better.“ \n– Jim Rohn");
    }

    @Override
    public String getMotivation() {
        int index = randomGenerator.nextInt(messages.size());
        return messages.get(index);
    }
}