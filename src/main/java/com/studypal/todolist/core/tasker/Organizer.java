package com.studypal.todolist.core.tasker;

import com.studypal.todolist.core.todo.TodoTask;

import java.time.LocalDateTime;

public class Organizer implements Comparable<Organizer> {

    private TodoTask todoTask;
    private String entryName;
    private String course;
    private LocalDateTime duedate;

    public Organizer(String entryName, String course, LocalDateTime duedate, TodoTask task) {
        this.entryName = entryName;
        this.course = course;
        this.duedate = duedate;
        this.todoTask = task;
    }

    public String getEntryName() {
        return entryName;
    }

    public void setEntryName(String entryName) {
        this.entryName = entryName;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    @Override
    public int compareTo(Organizer o) {
        if (getDuedate() == null || o.getDuedate() == null) {
            return 0;
        }
        return getDuedate().compareTo(o.getDuedate());
    }

    public LocalDateTime getDuedate() {
        return duedate;
    }

    public void setDuedate(LocalDateTime duedate) {
        this.duedate = duedate;
    }
}