package com.studypal.todolist.core.reminder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reminder")
public class Reminder {

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "owner_id", nullable = false)
    private String ownerId;

    @Column(name = "remindDate", columnDefinition = "DATE", nullable = false)
    private Date remindDate;

    @Column(name = "description", nullable = false)
    private String description;

    public Reminder() {
    }

    public Reminder(String ownerId, Date remindDate, String description) {
        this.ownerId = ownerId;
        this.remindDate = remindDate;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getOwnerId() {
        return ownerId;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = formatter.format(remindDate);
        return String.format("id=%d | %s | remind date: %s", this.id, this.description, formattedDate);
    }

    public static Date parseFromString(String fullDate) {
        String[] splittedDate = fullDate.split("-");
        TimeZone timeZone = TimeZone.getTimeZone("GMT+7");
        Calendar today = Calendar.getInstance(timeZone);
        int year = today.get(Calendar.YEAR);
        int month = today.get(Calendar.MONTH);
        int date = today.get(Calendar.DATE);
        switch (splittedDate.length) {
            case 1:
                date = Integer.parseInt(splittedDate[0]);
                break;
            case 2:
                month = Integer.parseInt(splittedDate[1]);
                date = Integer.parseInt(splittedDate[0]);
                break;
            case 3:
                year = Integer.parseInt(splittedDate[2]);
                month = Integer.parseInt(splittedDate[1]);
                date = Integer.parseInt(splittedDate[0]);
                break;
            default:
                throw new IndexOutOfBoundsException();
        }
        Date formattedDate = new GregorianCalendar(year, month, date).getTime();
        if (formattedDate.before(today.getTime())) {
            throw new IllegalArgumentException("Cannot set reminder to the past");
        }
        return formattedDate;
    }
}
