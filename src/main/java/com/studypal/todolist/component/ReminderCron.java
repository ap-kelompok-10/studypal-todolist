package com.studypal.todolist.component;

import com.studypal.todolist.core.reminder.Reminder;
import com.studypal.todolist.service.reminder.ReminderService;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.TextMessage;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class ReminderCron {

    @Autowired
    private ReminderService reminderService;

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Scheduled(cron = "0 0 6 * * *", zone = "GMT+7")
    public void sendReminder() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date today = calendar.getTime();
        List<Reminder> allDueReminder = reminderService.getReminderByDate(today);
        cronReplies(allDueReminder);
    }

    public void cronReplies(List<Reminder> allDueReminder) {
        for (Reminder reminder : allDueReminder) {
            try {
                String target = reminder.getOwnerId();
                TextMessage toSend = createTextMessage(reminder.getDescription());
                lineMessagingClient.pushMessage(new PushMessage(target, toSend)).get();
            } catch (InterruptedException | ExecutionException | NullPointerException e) {
                System.out.println("Error Ocurred");
            }
        }
    }

    protected static TextMessage createTextMessage(String message) {
        TextMessage textMessage = new TextMessage("reminder:\n" + message);
        return textMessage;
    }
}
