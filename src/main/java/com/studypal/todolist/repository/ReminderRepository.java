package com.studypal.todolist.repository;

import com.studypal.todolist.core.reminder.Reminder;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface ReminderRepository extends JpaRepository<Reminder, Integer> {
    @Query("select r from Reminder r where r.remindDate = :date")
    List<Reminder> findByDate(@Param("date") Date date);

    @Query("select r from Reminder  r where r.ownerId = :owner_id and r.remindDate > current_date order by r.remindDate asc")
    List<Reminder> findByOwnerId(@Param("owner_id") String ownerId);

    @Transactional
    @Modifying
    @Query("delete from Reminder r where r.id = :id and r.ownerId = :owner_id")
    void deleteByIdWithOwnerValidation(@Param("owner_id") String ownerId, @Param("id") int id);
}
