package com.studypal.todolist.repository;

import com.studypal.todolist.core.bot.Bot;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BotRepository extends JpaRepository<Bot, String> {
    @Query(value = "SELECT b FROM Bot b WHERE b.ownerId = :ownerId")
    Bot findBot(@Param("ownerId") String ownerId);
}
