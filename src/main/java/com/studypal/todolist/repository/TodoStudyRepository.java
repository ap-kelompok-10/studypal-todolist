package com.studypal.todolist.repository;

import com.studypal.todolist.core.todo.TodoStudy;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TodoStudyRepository extends JpaRepository<TodoStudy, String> {
    @Query(value = "SELECT s FROM TodoStudy s WHERE s.ownerId = :ownerId")
    List<TodoStudy> fetchStudies(@Param("ownerId") String ownerId);

    @Query(value = "SELECT s FROM TodoStudy s WHERE s.studyName = :studyName")
    TodoStudy findStudy(@Param("studyName") String studyName);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM TodoStudy s WHERE s.studyName = :studyName")
    void deleteStudy(@Param("studyName") String studyName);
}
