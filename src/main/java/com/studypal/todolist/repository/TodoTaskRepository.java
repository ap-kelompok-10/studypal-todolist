package com.studypal.todolist.repository;

import com.studypal.todolist.core.todo.TodoTask;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TodoTaskRepository extends JpaRepository<TodoTask, String> {
    @Query(value = "SELECT t FROM TodoTask t WHERE t.ownerId = :ownerId")
    List<TodoTask> fetchTasks(@Param("ownerId") String ownerId);

    @Query(value = "SELECT t FROM TodoTask t WHERE t.taskName = :taskName")
    TodoTask findTask(@Param("taskName") String taskName);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM TodoTask t WHERE t.taskName = :taskName")
    void deleteTask(@Param("taskName") String taskName);
}
